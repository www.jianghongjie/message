package com.message.ui.msg.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.msg.pojo.MsgInfo;
import com.message.admin.msg.service.MsgInfoService;
import com.message.admin.msg.service.MsgReceService;
import com.message.ui.msg.service.UiMsgInfoService;
import com.system.handle.model.ResponseFrame;

@Component
public class UiMsgInfoServiceImpl implements UiMsgInfoService {

	@Autowired
	private MsgInfoService msgInfoService;
	@Autowired
	private MsgReceService msgReceService;
	
	@Override
	public ResponseFrame pageQuery(MsgInfo msgInfo) {
		ResponseFrame frame = msgInfoService.pageQuery(msgInfo);
		/*@SuppressWarnings("unchecked")
		Page<MsgInfo> page = (Page<MsgInfo>) frame.getBody();
		for (MsgInfo mi : page.getRows()) {
			List<MsgRece> reces = msgReceService.findByMsgId(mi.getId());
			if(reces.size() > 0) {
				StringBuffer receUserIds = new StringBuffer();
				for (MsgRece mr : reces) {
					receUserIds.append(mr.getReceUserId()).append(";");
				}
				mi.setReceUserIds(receUserIds.toString());
			}
		}*/
		return frame;
	}

}
