package com.message.ui.msg.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.sys.pojo.UserInfo;
import com.message.admin.sys.service.SysInfoService;
import com.message.admin.sys.service.UserInfoService;
import com.message.ui.comm.controller.BaseController;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * userInfo的Controller
 * @author autoCode
 * @date 2017-12-27 15:24:04
 * @version V1.0.0
 */
@Controller
public class UiUserInfoController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UiUserInfoController.class);

	@Autowired
	private UserInfoService userInfoService;
	@Autowired
	private SysInfoService sysInfoService;
	
	@RequestMapping(value = "/userInfo/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		return "admin/msg/userInfo-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/userInfo/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo) {
		ResponseFrame frame = null;
		try {
			frame = userInfoService.pageQuery(userInfo);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/userInfo/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, String id) {
		if(FrameStringUtil.isNotEmpty(id)) {
			modelMap.put("userInfo", userInfoService.get(id));
		}
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		return "admin/msg/userInfo-edit";
	}
	
	@RequestMapping(value = "/userInfo/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response, UserInfo userInfo) {
		ResponseFrame frame = null;
		try {
			frame = userInfoService.saveOrUpdate(userInfo);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/userInfo/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		ResponseFrame frame = null;
		try {
			frame = userInfoService.delete(id);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
}