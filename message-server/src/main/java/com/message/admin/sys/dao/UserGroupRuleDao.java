package com.message.admin.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.message.admin.sys.pojo.UserGroupRule;

/**
 * user_group_rule的Dao
 * @author autoCode
 * @date 2017-12-26 15:20:29
 * @version V1.0.0
 */
public interface UserGroupRuleDao {

	public abstract void save(UserGroupRule userGroupRule);

	public abstract void update(UserGroupRule userGroupRule);

	public abstract void delete(@Param("id")String id);

	public abstract UserGroupRule get(@Param("id")String id);

	public abstract List<UserGroupRule> findUserGroupRule(UserGroupRule userGroupRule);
	
	public abstract int findUserGroupRuleCount(UserGroupRule userGroupRule);

	public abstract UserGroupRule getByGsu(@Param("groupId")String groupId, @Param("sysNo")String sysNo,
			@Param("userId")String userId);

	public abstract List<UserGroupRule> findBySysNoUserId(@Param("sysNo")String sysNo,
			@Param("userId")String userId);
}